package com.peterchaschke.finances;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;

public class ManualTransactionService extends Service {
	
	static String LOGNAME = "[ManualAdd]";

	private UI ui;
	private Repository repository;

	ManualTransactionService(UI ui, Repository repository) {
		this.ui = ui;
		this.repository = repository;
	}

	public void manualTrasactionLoad() {
		
		int institutionSel = 0;

		// Get all institutions from the database
		System.out.println("[INFO]"+LOGNAME+" Loading Institutions...");
		List<Institution> institutions = repository.getAllInstitutions();
		System.out.println("[INFO]"+LOGNAME+" Loaded Institutions");

		// Get all categories from the database
		System.out.println("[INFO]"+LOGNAME+" Loading Categories...");
		List<Category> categories = repository.getAllCategories();
		System.out.println("[INFO]"+LOGNAME+" Loaded Categories");

		// Parse list of categories and build display id map
		// Map: categoryId -> categoryName
		HashMap<Long, String> categoryDisplayNameMap = new HashMap<Long, String>();
		// Map: displayId -> categoryId
		HashMap<Integer, Long> categoryDisplayMap = new HashMap<Integer, Long>();
		int displayId = 1;
		for(Category category : categories) {
			categoryDisplayMap.put(displayId, category.getId());
			categoryDisplayNameMap.put(category.getId(), category.getName());
			category.setDisplayId(displayId);
			displayId++;
		}
		
		// Build institution menu items array
		ArrayList<String> institutionMenuItems = new ArrayList<>();
		for(int i = 0; i<institutions.size(); i++) {
			institutionMenuItems.add(Integer.toString(i+1));
			institutionMenuItems.add(institutions.get(i).getName());
		}

		// Get the institution
		institutionSel = ui.getMenuSelection("Select Institution", "Which Institution?", institutionMenuItems, "16", "60", "5");
		if(institutionSel <= 0) {
			// Error, go back to main menu
			System.out.println("[ERROR]"+LOGNAME+" Error Getting Selected Insititution");
			ui.displayInformation("Cancelled Adding Transaction.");
			return;
		}
		
		Institution institution =  institutions.get(institutionSel-1);
		
		Transaction transaction = new Transaction();
		transaction.setInstitutionId(institution.getId());
		
		// Select Transaction Date
		String message = "For the transaction at "+institution.getName() + ":\nEnter the date of the transaction (MM/dd/yyyy)";
		String inputFormat = "MM/dd/yyyy";
		
		Date date = ui.getInputDate("Input Date", message, "", inputFormat, null, null, "10", "60");
		if(date == null) {
			ui.displayInformation("Cancelled Adding Transaction.");
			return;
		}
		String strDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
		transaction.setDate(strDate);

		
		// Select Transaction Description
		String transactionText = "For the transaction on " + strDate + " at "+institution.getName() + ":\n\nEnter Transaction Desciption";
		String description = ui.getInputString("Transaction Description", transactionText, "", "10", "60", "");
		if(description == null) {
			ui.displayInformation("Cancelled Adding Transaction.");
			return;
		}
		transaction.setDescription(description);
		
		// Select Transaction Amount
		
		message = "For the transaction on " + transaction.getDate() + " at "+institution.getName() + "\n" + transaction.getDescription() + "\n\nEnter the amount of the transaction (Greater than 0)";
		
		BigDecimal amount = ui.getInputBigDecimal("Select Amount", message, "", BigDecimal.ZERO, null, BigDecimal.ZERO, "15", "60");
		
		//BigDecimal amount =  ui.displayTransactionAmountInput(transaction, institution.getName());
		if(amount == null) {
			ui.displayInformation("Cancelled Adding Transaction.");
			return;
		}
		transaction.setAmount(amount);
		
		// Select Transaction Category
		if(categories.size() <= 0) {
			ui.displayInformation("No loaded categories, exiting.");
			return;
		}
		
		// Build the menu of categories
		ArrayList<String> categoryMenuItems = new ArrayList<>(Arrays.asList());
		for(Category category : categories) {
			categoryMenuItems.add(String.valueOf(category.getDisplayId()));
			categoryMenuItems.add(category.getName());
		}
		message = transaction.getDate()+" | $"+ String.format("%.2f", transaction.getAmount()) +" | "+ transaction.getDescription();
		int categorySel = ui.getMenuSelection("Categorize Transaction", message, categoryMenuItems, "18", "60", "8");
		if(categorySel < 0) {
			ui.displayInformation("Cancelled Adding Transaction.");
			return;
		}
		
		transaction.setCategoryId(categoryDisplayMap.get(categorySel));
		
		// Approve Import of Transaction
		
		transactionText = "["+ categoryDisplayNameMap.get(categoryDisplayMap.get(categorySel)) +"] "+ institution.getName() + " | " + strDate + " | $" + String.format("%,.2f", amount) + "\n" + description;
		
		int addSel = ui.displayYesNo("Import Transaction", transactionText+"\n\nDo you really want to add this transaction?");
		if(addSel < 0) { // User selected yes (1)
			ui.displayInformation("Cancelled Adding Transaction.");
			return;
		}
		
		// Check Hash Against DB
		String hash = getHashValue(transaction);
		transaction.setHash(hash);
		
		EntityManager em = repository.getEntityManager();
		List<Transaction> matchingTransactions = repository.getTransactionByHash(em, transaction.getHash());
		
		if(matchingTransactions != null) {
			if(matchingTransactions.size() > 0) {
				
				// Matching transaction
				// Display repeat transaction dialog
				Transaction matchingTransaction = matchingTransactions.get(0);
				
				ArrayList<String> repeatTransactionMenuItems = new ArrayList<String>(Arrays.asList("1", "Skip Transaction", "2", "Update Existing", "3", "Add Anyways"));
				
				boolean exitRepeatTransactionMenu = false;
				while(!exitRepeatTransactionMenu) {
					
					String existingTransaction = matchingTransaction.getDate() + "| $" + String.format("%,.2f", matchingTransaction.getAmount()) + "|" + matchingTransaction.getDescription();
					String repeatTransaction = transaction.getDate() + "| $" + String.format("%,.2f", transaction.getAmount()) + "|" + transaction.getDescription();
					message = "Identified a existing transaction in the database. What do you want to do?\n\nExisting: "+existingTransaction+"\nNew Transaction: " + repeatTransaction;
					
					int repeatTransactionSel = ui.getMenuSelection("Existing Transaction", message, repeatTransactionMenuItems, "18", "60", "4");
					
					if(repeatTransactionSel == 2) {
						// Update existing transaction
						repository.updateTransaction(em, transaction, matchingTransaction.getId());
						exitRepeatTransactionMenu = true;
						
					} else if(repeatTransactionSel == 3) {
						// Add identified repeat transaction anyways
						repository.addTransaction(em, transaction);
						exitRepeatTransactionMenu = true;
						
					} else {
						// Skip repeat transaction
						exitRepeatTransactionMenu = true;
					}
				}
				
			} else {
				// Add Transaction into DB
				repository.addTransaction(em, transaction);
			}
		}
		// Close open entity manager
		em.close();
		
		// Display success message
		ui.displayInformation("Successfully imported transactions.");
		
		return;
	}

}
