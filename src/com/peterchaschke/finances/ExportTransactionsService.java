package com.peterchaschke.finances;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

public class ExportTransactionsService {
	
	static String LOGNAME = "[Export]";

	private UI ui;
	private Repository repository;

	ExportTransactionsService(UI ui, Repository repository) {
		this.ui = ui;
		this.repository = repository;
	}

	public void exportTransactions() {
		
		// Get all institutions from the database
		System.out.println("[INFO]"+LOGNAME+" Loading Institutions...");
		List<Institution> institutions = repository.getAllInstitutions();
		System.out.println("[INFO]"+LOGNAME+" Loaded Institutions");
		
		// Get date range
		String inputFormat = "MM/dd/yyyy";
		
		// Select Start Transaction Date
		String message = "Enter begin date for records. (MM/dd/yyyy)";
		
		Date startDate = ui.getInputDate("Input Starting Date", message, "", inputFormat, null, null, "10", "60");
		if(startDate == null) {
			ui.displayInformation("Cancelled file export");
			return;
		}
		String strStartDate = new SimpleDateFormat("yyyy-MM-dd").format(startDate);
		
		// Select End Transaction Date
		message = "Enter ending date for records. (MM/dd/yyyy)";
		
		Date endDate = ui.getInputDate("Input Ending Date", message, "", inputFormat, startDate, null, "10", "60");
		if(endDate == null) {
			ui.displayInformation("Cancelled file export");
			return;
		}
		String strEndDate = new SimpleDateFormat("yyyy-MM-dd").format(endDate);
		
		System.out.println(strStartDate +" - "+strEndDate);
		
		
		// Select (Deselect) institutions
		// Build institution menu items array
		ArrayList<String> institutionChecklistItems = new ArrayList<>();
		for(int i = 0; i<institutions.size(); i++) {
			institutionChecklistItems.add(String.valueOf(institutions.get(i).getId()));
			institutionChecklistItems.add(institutions.get(i).getName());
			institutionChecklistItems.add("ON");
		}
		
		String[] checklistSel = ui.getChecklistSelection("Select Institutions", "Select the institutions included in the export.", institutionChecklistItems, true, "15", "60", "4");
		if(checklistSel == null) {
			ui.displayInformation("Cancelled file export");
			return;
		}
		
		// Convert String array to integer list
		ArrayList<Long> institutionIds = new ArrayList<>();
		//System.out.println("Checklist Select Length: "+checklistSel.length);
		for(int i = 0; i<checklistSel.length; i++) {
			//System.out.println(Integer.parseInt(checklistSel[i].replace("\"", "")));
			institutionIds.add(Long.parseLong(checklistSel[i].replace("\"", "")));
		}
		
		// Enter filename
		// Auto fill suggestion
		String filenameSuggestion = "../../Documents/Finances/" + strStartDate + "_" + strEndDate + "_Finances_Export.csv";
		String fileName = ui.getInputString("Export File Name", "What the export file? (.csv)", filenameSuggestion, "10", "60", "");
		if(fileName == null) {
			ui.displayInformation("Cancelled file export");
			return;
		}
		// Add .csv file extension if it's not included.
		if(!fileName.trim().endsWith(".csv")) {
			fileName = fileName.replaceAll("[\\:*?\"<>|]", "");
			fileName = fileName+".csv";
		}
		
		System.out.println(fileName);
		
		// Export transactions into object array
		// Open entity manager
		EntityManager em = repository.getEntityManager();
		
		List<Transaction> transactions = repository.getTransactionByDate(em, startDate, endDate, institutionIds);
		
		// Close open entity manager
		em.close();
		
		if(transactions == null) {
			ui.displayInformation("Error retrieving transactions. Cancelled file export.");
			return;
		}
		
		if(transactions.size() <= 0) {
			ui.displayInformation("No records found. Cancelled file export.");
			return;
		}
		
		String[] headerValues = {"ID", "InstitutionId", "CategoryId", "Date", "Amount", "Description", "Note"};
		
		char DEFAULT_SEPARATOR = ','; 
		
		// Write csv file
		try {
			
			File file = new File(fileName);
			
			file.createNewFile();
			
			FileWriter writer = new FileWriter(file);
			
			StringBuilder sb = new StringBuilder();
			
			for(int i = 0; i < headerValues.length; i++) {
				sb.append(headerValues[i]);
				if(i+1 == headerValues.length) {
					continue;
				}
				sb.append(DEFAULT_SEPARATOR);
			}
			sb.append("\n");
			
			for(Transaction transaction : transactions) {
				sb.append(transaction.getId());
				sb.append(DEFAULT_SEPARATOR);
				sb.append(transaction.getInstitutionId());
				sb.append(DEFAULT_SEPARATOR);
				sb.append(transaction.getCategoryId());
				sb.append(DEFAULT_SEPARATOR);
				sb.append(transaction.getDate());
				sb.append(DEFAULT_SEPARATOR);
				sb.append(transaction.getAmount());
				sb.append(DEFAULT_SEPARATOR);
				sb.append(transaction.getDescription());
				sb.append(DEFAULT_SEPARATOR);
				if(transaction.getNote() != null) {
					sb.append(transaction.getNote());
				} else {
					sb.append("");
				}
				sb.append("\n");
			}
			
			writer.append(sb.toString());
			
			writer.flush();
	        writer.close();
			
		} catch (IOException e) {
			
			e.printStackTrace();
			ui.displayInformation("Error creating export file.");
		}
		
		// Display success/error message
		ui.displayInformation("Successfully created export file.");
		
		
		return;
	}

}
