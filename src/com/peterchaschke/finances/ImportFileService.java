package com.peterchaschke.finances;

import java.io.File;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;

public class ImportFileService extends Service {

	static String LOGNAME = "[ImportFile]";

	private UI ui;
	private Repository repository;

	ImportFileService(UI ui, Repository repository) {
		this.ui = ui;
		this.repository = repository;
	}

	public void importFile(String enteredFilePath) {

		int institutionSel = 0;
		String file = null;
		int year = 0;
		int internalTransactionId = 0;
		String errorMsg = "";

		// Get all institutions from the database
		System.out.println("[INFO]"+LOGNAME+" Loading Institutions...");
		List<Institution> institutions = repository.getAllInstitutions();
		System.out.println("[INFO]"+LOGNAME+" Loaded Institutions");

		// Get all categories from the database
		System.out.println("[INFO]"+LOGNAME+" Loading Categories...");
		List<Category> categories = repository.getAllCategories();
		System.out.println("[INFO]"+LOGNAME+" Loaded Categories");
		
		// Build institution menu items array
		ArrayList<String> institutionMenuItems = new ArrayList<>();
		for(int i = 0; i<institutions.size(); i++) {
			institutionMenuItems.add(Integer.toString(i+1));
			institutionMenuItems.add(institutions.get(i).getName());
		}

		// Get the institution
		institutionSel = ui.getMenuSelection("Select Institution", "Which Institution?", institutionMenuItems, "16", "60", "5");
		if(institutionSel <= 0) {
			// Error, go back to main menu
			ui.displayInformation("Cancelling importing transaction file.");
			System.out.println("[ERROR]"+LOGNAME+" Error Getting Selected Insititution");
			return;
		}

		Institution institution =  institutions.get(institutionSel-1);

		System.out.println("[INFO]"+LOGNAME+" Selected Insititution: "+institution.getName());

		if(enteredFilePath.isEmpty()) {
			enteredFilePath = "DataLoads/";
		}
		System.out.println("[INFO]"+LOGNAME+" Entered Filepath: "+enteredFilePath);
		// Get the input file name TODO: validate?
		boolean validInputFile = false;
		while(!validInputFile) {
			file = null;
			file = ui.getInputString("Input File Name", "What the input file? (.csv)", enteredFilePath, "10", "60", errorMsg);
			
			try {
				File f = new File(file);
				if(f.exists() && !f.isDirectory()) {
					errorMsg = "";
					validInputFile = true;
				}
			} catch(NullPointerException e) {
				file = null;
				errorMsg = "";
				validInputFile = true;
			}
			enteredFilePath = file;
			errorMsg = "Input file is not valid.";
			
		}
		if(file == null) {
			ui.displayInformation("Cancelling importing transaction file.");
			return;
		}

		// Get year of records if "no year is specified in the file format
		if(!institution.getFileFormat().toUpperCase().contains("YY")) {
			System.out.println("[INFO]"+LOGNAME+" Selected institution file format does not include year");
			// Get the specific institution
			
			
			// Select year of records
			String message = "What the year of the record(s) (yyyy)";
			String inputFormat = "yyyy";
			
			Date date = ui.getInputDate("Input Year", message, "", inputFormat, null, null, "10", "60");
			if(date == null) {
				return;
			}
			year = Integer.parseInt(new SimpleDateFormat("yyyy").format(date));
			
			System.out.println("[INFO]"+LOGNAME+" Using entered year: " + year);
		}

		System.out.println("[INFO]"+LOGNAME+" Parsing file: "+file);

		// Parse the csv input file
		CSVParser parser = new CSVParser(institution);

		ArrayList<Transaction> transactions = parser.parseFile(file, year);

		// Add category to indicate to not include the transaction
		categories.add(new Category(-1, "DO NOT INCL"));

		// Parse list of categories and build display id map
		// Map: categoryId -> categoryName
		HashMap<Long, String> categoryDisplayNameMap = new HashMap<Long, String>();
		// Map: displayId -> categoryId
		HashMap<Integer, Long> categoryDisplayMap = new HashMap<Integer, Long>();
		int displayId = 1;
		for(Category category : categories) {
			categoryDisplayMap.put(displayId, category.getId());
			categoryDisplayNameMap.put(category.getId(), category.getName());
			category.setDisplayId(displayId);
			displayId++;
		}
		// Build the menu of categories
		ArrayList<String> categoryMenuItems = new ArrayList<>();
		for(Category category : categories) {
			categoryMenuItems.add(String.valueOf(category.getDisplayId()));
			categoryMenuItems.add(category.getName());
		}

		// Categorize all loaded transactions
		if(categories.size() > 0) {
			// Loop through transactions and categorize them
			int currentTransaction = 1;

			for(Transaction transaction : transactions) {

				int categorySel = 0;
				boolean exitCategorize = false;
				while(!exitCategorize) {
					
					String message = "("+currentTransaction+"/"+transactions.size()+")\n "+transaction.getDate()+" | $"+ String.format("%.2f", transaction.getAmount()) +" | "+ transaction.getDescription();
					
					categorySel = ui.getMenuSelection("Categorize Transactions", message, categoryMenuItems, "18", "60", "8");
					if(categorySel < 0) {
						transaction.setCategoryId(-1);
						exitCategorize = true;
						continue;
					}
					
					// Set transaction institution
					transaction.setInstitutionId(institution.getId());
					
					// Set internal transaction id
					transaction.setInternalId(internalTransactionId++);
					
					String hash = getHashValue(transaction);
					transaction.setHash(hash);
					
					
					// Set transaction category and whether or not to include it
					if(categorySel == displayId-1) {
						transaction.setIncludeFlag(false);
					} else {
						transaction.setIncludeFlag(true);
					}
					transaction.setCategoryId(categoryDisplayMap.get(categorySel));

					exitCategorize = true;

				}

				currentTransaction++;
			}
		} else {
			System.out.println("[INFO]"+LOGNAME+" No categories, skipping categorization");
			return;
		}

		boolean exitTransactionMenu = false;
		
		ArrayList<String> menuItems = new ArrayList<String>(Arrays.asList("1", "Review/Edit Transactions", "2", "Import To Database", "3", "Cancel Import"));
		
		while(!exitTransactionMenu) {
			int transactionMenuSel = ui.getMenuSelection("Transactions Menu", "What would you like to do with the loaded transactions?", menuItems, "15", "60", "4");

			switch(transactionMenuSel) {
			case 1:
				// Review/Edit Transactions

				boolean exitParsedTransactionMenu = false;
				while(!exitParsedTransactionMenu) {

					// Build parsed transaction menu items array
					ArrayList<String> parsedTransactionsMenuItems = new ArrayList<>();
					int i = 0;
					for(Transaction transaction : transactions) {
						parsedTransactionsMenuItems.add(Integer.toString(i+1));
						
						String strDate = "";
						try {
							Date date;
							date = new SimpleDateFormat("yyyy-MM-dd").parse(transaction.getDate());
							strDate = new SimpleDateFormat("MM/dd").format(date);
						} catch (ParseException e) {
							strDate = transaction.getDate();
						}
						
						parsedTransactionsMenuItems.add("[" +categoryDisplayNameMap.get(transaction.getCategoryId())+ "] " + strDate + "|" + transaction.getDescription());
						i++;
					}
					
					int transactionSel = ui.getMenuSelection("Review/Edit Transactions", "Select a Transaction", parsedTransactionsMenuItems, "20", "70", "12");
					
					if(transactionSel < 1) {
						exitParsedTransactionMenu = true;
						continue;
					}
					Transaction selectedTransaction = transactions.get(transactionSel-1);		

					boolean exitSelectedTransactionMenu = false;
					while(!exitSelectedTransactionMenu) {
						
						// Build view/edit transaction menu list
						ArrayList<String> viewTransactionMenuItems = new ArrayList<>(Arrays.asList("1", "Recategorize"));
						
						// Do not allow split transactions to be split again
						if(selectedTransaction.getNote() == null) {
							viewTransactionMenuItems.add("2");
							viewTransactionMenuItems.add("Split");
						} else {
							// Only allow split transactions to be deleted
							if(selectedTransaction.getNote().startsWith("[Split]")) {
								viewTransactionMenuItems.add("3");
								viewTransactionMenuItems.add("Delete Split Transaction");
							}
						}
						viewTransactionMenuItems.add("4");
						viewTransactionMenuItems.add("Back");
						
						String message = "["+ categoryDisplayNameMap.get(selectedTransaction.getCategoryId()) +"]\n" + selectedTransaction.getDate() + " | $" + String.format("%.2f", selectedTransaction.getAmount()) + "\n" + selectedTransaction.getDescription();
						
						int selectedTransactionMenuSel = ui.getMenuSelection("View/Edit Transaction", message, viewTransactionMenuItems, "15", "60", "4");
						//int selectedTransactionMenuSel = ui.displayTransactionMenu(selectedTransaction, categoryDisplayNameMap.get(selectedTransaction.getCategoryId()));

						if(selectedTransactionMenuSel < 1) {
							exitSelectedTransactionMenu = true;
							continue;
						}

						switch(selectedTransactionMenuSel) {
						case 1:
							// Re-categorize
							int recategorizeSel = 0;
							boolean exitRecategorization = false;
							while(!exitRecategorization) {
								
								message = selectedTransaction.getDate()+" | $"+ String.format("%.2f", selectedTransaction.getAmount()) +" | "+ selectedTransaction.getDescription();
								
								recategorizeSel = ui.getMenuSelection("Categorize Transaction", message, categoryMenuItems, "18", "60", "8");
								if(recategorizeSel < 0) {
									exitRecategorization = true;
									continue;
								}
								// Set transaction category and whether or not to include it
								if(recategorizeSel == displayId-1) {
									selectedTransaction.setIncludeFlag(false);
								} else {
									selectedTransaction.setIncludeFlag(true);
								}
								selectedTransaction.setCategoryId(categoryDisplayMap.get(recategorizeSel));

								exitRecategorization = true;
							}
							break;
						case 2:
							// Split

							// Enter Description
							String transactionText = "[Parent Transaction] \n"+ categoryDisplayNameMap.get(selectedTransaction.getCategoryId()) 
									+" | " + selectedTransaction.getDate() + " | $" + String.format("%.2f", selectedTransaction.getAmount()) + "\n" 
									+ selectedTransaction.getDescription() +"\n\nEnter the description for the split transaction.";
							
							String splitDescription = ui.getInputString("Split Transaction Description", transactionText, "", "13", "60", "");
							if(splitDescription == null) {
								ui.displayInformation("Cancelled splitting transaction.");
								break;
							}

							// Enter amount
							BigDecimal selectedAmount = new BigDecimal("-1");
							boolean exitSplitAmount = false;
							while(!exitSplitAmount) {
								
								message = "[Parent Trans Amount]: $" + String.format("%.2f", selectedTransaction.getAmount().abs()) 
									+ "\n[Split Trans Description]: " + splitDescription + "\n\nEnter the amount for the split transaction.";
								
								selectedAmount = ui.getInputBigDecimal(
										"Select Amount", 
										message, 
										"", 
										BigDecimal.ZERO,
										selectedTransaction.getAmount().abs(),
										selectedTransaction.getAmount().abs(),
										"15", 
										"60");
								if(selectedAmount == null) {
									ui.displayInformation("Cancelled splitting transaction.");
									exitSplitAmount = true;
									continue;
								}
								
								// Select category
								int splitTransactionSel = 0;
								boolean exitSplitCategorize = false;
								while(!exitSplitCategorize) {
									
									message = "Split Transaction: " + selectedTransaction.getDate()+" | $-"+ String.format("%,.2f", selectedAmount) + " | " + splitDescription + "\n" +
											"Parent Transaction: " + selectedTransaction.getDate()+" | $"+ String.format("%.2f", selectedTransaction.getAmount()) +" | "+ selectedTransaction.getDescription();
									splitTransactionSel = ui.getMenuSelection("Categorize Split Transaction", message, categoryMenuItems, "18", "60", "8");

									if(splitTransactionSel < 0) {
										ui.displayInformation("Cancelled splitting transaction.");
										exitSplitCategorize = true;
										continue;
									}

									boolean includeFlag = true;
									// Set transaction category and whether or not to include it
									if(splitTransactionSel == displayId-1) {
										includeFlag = false;
									}
									// Create split transaction
									Transaction splitTransaction = new Transaction(
											institution.getId(), 
											categoryDisplayMap.get(splitTransactionSel), 
											selectedTransaction.getDate(), 
											splitDescription, 
											selectedAmount.negate(),
											"[Split]|"+selectedTransaction.getInternalId()+"|"+selectedTransaction.getDescription().trim()+"|"+selectedTransaction.getHash().trim()+"|", 
											internalTransactionId++,
											includeFlag
											);
									
									splitTransaction.setHash(getHashValue(splitTransaction));

									// Add the transaction into the list
									transactions.add(transactionSel, splitTransaction);

									// Add split transaction amount from selected transaction
									// Split value is positive, parent amount is negative
									transactions.get(transactionSel-1).setAmount(selectedTransaction.getAmount().add(selectedAmount));
									
									ui.displayInformation("Successfully added split transaction.");
									
									exitSplitCategorize = true;
								}
								exitSplitAmount = true;
							}

							break;
							
						case 3:
							// Delete Transaction
							int deleteSelection = ui.displayYesNo("Delete Split Transaction", "Do you really want to delete this split transaction?");
							if(deleteSelection > 0) { // User selected yes (1)
								int parentId = Integer.parseInt(transactions.get(transactionSel-1).getNote().split("\\|")[1].trim());
								transactions.get(parentId).addAmount(selectedTransaction.getAmount());
								transactions.remove(transactionSel-1);
								ui.displayInformation("Deleted split transaction.");
								exitSelectedTransactionMenu = true;
								continue;
							}
							break;
							

						default:
							// Exit selected transaction
							exitSelectedTransactionMenu = true;
							break;
						}
					}

				} // End displayParsedTransactions List while loop
				
				break; // End Review/Edit Transaction case

			case 2: // transactionMenuSel case
				// Import Transactions to Database
				System.out.println("[INFO]"+LOGNAME+" Import Transactions");
				
				// Check for matching hash numbers in DB table for each entry.
				
				EntityManager em = repository.getEntityManager();
				
				for(Transaction transaction : transactions) {
					
					// Check for DO NOT INCL type
					if(transaction.getCategoryId() != -1) {
					
						List<Transaction> matchingTransactions = repository.getTransactionByHash(em, transaction.getHash());
					
						if(matchingTransactions != null) {
							if(matchingTransactions.size() > 0) {
								
								// Matching transaction
								// Display repeat transaction dialog
								Transaction matchingTransaction = matchingTransactions.get(0);
								
								ArrayList<String> repeatTransactionMenuItems = new ArrayList<String>(Arrays.asList("1", "Skip Transaction", "2", "Update Existing", "3", "Add Anyways"));
								
								boolean exitRepeatTransactionMenu = false;
								while(!exitRepeatTransactionMenu) {
									
									String existingTransaction = matchingTransaction.getDate() + "| $" + String.format("%,.2f", matchingTransaction.getAmount()) + "|" + matchingTransaction.getDescription();
									String repeatTransaction = transaction.getDate() + "| $" + String.format("%,.2f", transaction.getAmount()) + "|" + transaction.getDescription();
									String message = "Identified a existing transaction in the database. What do you want to do?\n\nExisting: "+existingTransaction+"\nNew Transaction: " + repeatTransaction;
									
									int repeatTransactionSel = ui.getMenuSelection("Existing Transaction", message, repeatTransactionMenuItems, "18", "60", "4");
									
									//int repeatTransactionSel = ui.displayRepeatTransactionMenu(matchingTransaction, transaction);
									if(repeatTransactionSel == 2) {
										// Update existing transaction
										repository.updateTransaction(em, transaction, matchingTransaction.getId());
										exitRepeatTransactionMenu = true;
										
									} else if(repeatTransactionSel == 3) {
										// Add identified repeat transaction anyways
										repository.addTransaction(em, transaction);
										exitRepeatTransactionMenu = true;
										
									} else {
										// Skip repeat transaction
										exitRepeatTransactionMenu = true;
									}
								}
								continue;
								
							} else {
								repository.addTransaction(em, transaction);
							}
						}
					} // End DO NOT INCL CHECK
				}
				// Close open entity manager
				em.close();
				
				// Display success message
				ui.displayInformation("Successfully imported transactions from file.");
				
				exitTransactionMenu = true;
				break;

			default:
				// Exit case
				ui.displayInformation("Cancelling file import.");
				exitTransactionMenu = true;
			}
		} // End while loop for transaction menu

	}

}
