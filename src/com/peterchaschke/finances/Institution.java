package com.peterchaschke.finances;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity (name = "institutions")
public class Institution {
	
	@Id
	private Long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "file_format")
	private String fileFormat;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getFileFormat() {
		return fileFormat;
	}
	
	public void setFileFormat(String fileFormat) {
		this.fileFormat = fileFormat;
	}
	
}
