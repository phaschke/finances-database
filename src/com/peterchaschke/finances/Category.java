package com.peterchaschke.finances;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity(name = "categories")
public class Category {
	
	@Id
	private long id;
	
	@Column(name = "name")
	private String name;
	
	@Transient
	private int displayId;
	
	Category() {}
	
	Category(long id, String name) {
		this.setId(id);
		this.setName(name);
	}
	
	Category(long id, String name, int displayId) {
		this.setId(id);
		this.setName(name);
		this.setDisplayId(displayId);
	}
	
	public long getId() {
		return this.id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getDisplayId() {
		return this.displayId;
	}
	
	public void setDisplayId(int displayId) {
		this.displayId = displayId;
	}

}
