package com.peterchaschke.finances;

public class Finances {
	
	public static void main(String[] args) {
		
		WhiptailUI whiptail = new WhiptailUI();
		FinancesRepository financesRepository = new FinancesRepository();
		
		String enteredFilePath = "";
		if(args.length > 0) {
			enteredFilePath = args[0];
		}
		
		Application app = new Application(whiptail, financesRepository);
		app.start(enteredFilePath);
	}
	
}
