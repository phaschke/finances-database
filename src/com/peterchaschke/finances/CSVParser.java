package com.peterchaschke.finances;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONObject;

public class CSVParser {

	private String dateFormat;
	private int datePos;
	private int descriptionPos;
	private int amountPos;
	private boolean negateAmount;
	private boolean skipHeader;

	CSVParser(Institution institution) {

		JSONObject jo = new JSONObject(institution.getFileFormat());

		this.dateFormat = jo.get("date_format").toString();
		this.datePos = jo.getInt("date_col");
		this.descriptionPos = jo.getInt("description_col");
		this.amountPos = jo.getInt("amount_col");
		this.negateAmount = jo.getBoolean("negate_amount");
		this.skipHeader = jo.getBoolean("skip_header");

	}

	public ArrayList<Transaction> parseFile(String csvFile, int year) {

		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		String dateStr = "";
		String format = dateFormat.replace("\"", "").trim();
		
		ArrayList<Transaction> transactions = new ArrayList<Transaction>();
		
		if(year > 0) {
			format = format+"/yyyy";
		}

		try {

			br = new BufferedReader(new FileReader(csvFile));
			int iteration = 0;
			while ((line = br.readLine()) != null) {
				
				if(skipHeader && iteration == 0) {
					iteration++;
					continue;
				}

				// use comma as separator
				String[] transaction = line.split(cvsSplitBy);
				
				dateStr = transaction[datePos].replace("\"", "").trim();
				if(year > 0) {
					dateStr = dateStr+"/"+year;
				}
				
				Date date = new SimpleDateFormat(format).parse(dateStr);
				String strDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
				
				BigDecimal amount = new BigDecimal(transaction[amountPos].replace("\"", "").trim());
				if(negateAmount) {
					amount = amount.negate();
				}

				transactions.add(new Transaction(strDate, transaction[descriptionPos].replace("\"", ""), amount));
				
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return transactions;
	}

}
