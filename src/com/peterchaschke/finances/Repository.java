package com.peterchaschke.finances;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

public interface Repository {
	
	EntityManager getEntityManager();
	
	List<Institution> getAllInstitutions();
	
	List<Category> getAllCategories();
	
	List<Transaction> getTransactionByHash(EntityManager em, String hash);
	
	List<Transaction> getTransactionByDate(EntityManager em, Date startDate, Date endDate, List<Long> institutions);
	
	void addTransaction(EntityManager em, Transaction transaction);
	
	void updateTransaction(EntityManager em, Transaction transaction, Long matchingTransactionId);
}
