package com.peterchaschke.finances;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class FinancesRepository implements Repository {
	
	/*EntityManager em;
	
	FinancesRepository() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("my-persistence-unit");
		this.em = emf.createEntityManager();
	}*/
	
	public EntityManager getEntityManager() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("my-persistence-unit");
		return emf.createEntityManager();
	}
	
	public List<Institution> getAllInstitutions() {
		
		List<Institution> institutions = null;
		EntityManager em = getEntityManager();
		try {
			institutions = em.createQuery("from institutions", Institution.class).getResultList();
			em.close();
			return institutions;
			
		} catch (Exception e) {
			
			em.close();
			System.out.println(e.getMessage());
			return institutions;
		}
		
	}
	
	public List<Category> getAllCategories() {
		
		List<Category> categories = null;
		EntityManager em = getEntityManager();
		try {
			categories = em.createQuery("from categories c ORDER BY list_order DESC", Category.class).getResultList();
			em.close();
			return categories;
			
		} catch (Exception e) {
			
			em.close();
			System.out.println(e.getMessage());
			return categories;

		}
	}
	
	public List<Transaction> getTransactionByHash(EntityManager em, String hash) {
		
		List<Transaction> transactions = null;

		try {
			transactions = em.createQuery("SELECT t FROM transactions t WHERE t.hash LIKE :hash", Transaction.class)
					.setParameter("hash", hash)
					.getResultList();
			
			return transactions;
			
		} catch (Exception e) {

			System.out.println(e.getMessage());
			return transactions;

		}
	}
	
	public List<Transaction> getTransactionByDate(EntityManager em, Date startDate, Date endDate, List<Long> institutions) {
		
		List<Transaction> transactions = null;

		try {
			transactions = em.createQuery("SELECT t FROM transactions t WHERE t.date BETWEEN :startDate AND :endDate AND t.institutionId IN :institutions", Transaction.class)
					.setParameter("startDate", new SimpleDateFormat("yyyy-MM-dd").format(startDate))
					.setParameter("endDate", new SimpleDateFormat("yyyy-MM-dd").format(endDate))
					.setParameter("institutions", institutions)
					.getResultList();
			
			return transactions;
			
		} catch (Exception e) {

			System.out.println(e.getMessage());
			return transactions;

		}
	}
	
	public void addTransaction(EntityManager em, Transaction transaction) {
		em.getTransaction().begin();
		em.persist(transaction);
		em.getTransaction().commit();
		
	}
	
	public void updateTransaction(EntityManager em, Transaction transaction, Long matchingTransactionId) {
		em.getTransaction().begin();
		em.createQuery("UPDATE transactions t "
				+ "SET t.categoryId = :categoryId, t.description = :description, t.amount = :amount WHERE "
				+ "t.id = :id")
			.setParameter("categoryId", transaction.getCategoryId())
			.setParameter("description", transaction.getDescription())
			.setParameter("amount", transaction.getAmount())
			.setParameter("id", matchingTransactionId)
			.executeUpdate();
		em.getTransaction().commit();
		
	}
}
