package com.peterchaschke.finances;

public class Service {
	
	public String getHashValue(Transaction transaction) {
		return transaction.getDate().trim()+String.format("%,.2f", transaction.getAmount())+transaction.getDescription().trim().hashCode();
	}

}
