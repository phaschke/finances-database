package com.peterchaschke.finances;

import java.util.ArrayList;
import java.util.Arrays;

public class Application {
	
	static String LOGNAME = "[MainMenu]";
	
	private UI ui;
	private Repository repository;
	
	Application(UI ui, Repository repository) {
		this.ui = ui;
		this.repository = repository;
	}
	
	void start(String enteredFilePath) {
		System.out.println("[INFO] Transaction Loader v1.0");
		
		ArrayList<String> menuItems = new ArrayList<String>(Arrays.asList("1", "Import File", "2", "Load Manual Transaction", "3", "Export Transactions", "4", "Quit"));
		
		boolean exitMainMenu = false;
		while(!exitMainMenu) {
			
			int menuResult = ui.getMenuSelection("Transactions Main Menu", "What would you like to do?", menuItems, "15", "60", "4");
			
			switch(menuResult) {
				case 1:
					// Load file
					System.out.println("[INFO]"+LOGNAME+" Begin file import");
					
					ImportFileService importFileService = new ImportFileService(ui, repository);
					importFileService.importFile(enteredFilePath);
					continue;
					
				case 2:
					// Add single transaction
					System.out.println("[INFO] "+LOGNAME+" Begin add single transaction");
					
					ManualTransactionService manualTransactionService = new ManualTransactionService(ui, repository);
					manualTransactionService.manualTrasactionLoad();
					continue;
					
				case 3:
					// Export transactions
					System.out.println("[INFO] "+LOGNAME+" Begin export transactions");
					
					ExportTransactionsService exportTransactionsService = new ExportTransactionsService(ui, repository);
					exportTransactionsService.exportTransactions();
					continue;
					
				default:
					// Error
					exitMainMenu = true;
					break;
			}
		}
		System.out.println("Bye!");
		System.exit(0);
	}
}
