package com.peterchaschke.finances;

public class ProcessResult {
	
	private int errorCode;
	private String result;
	
	ProcessResult(int errorCode, String result) {
		
		this.errorCode = errorCode;
		this.result = result;
	}
	
	int getErrorCode() {
		return this.errorCode;
	}
	
	void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	
	String getResult() {
		return this.result;
	}
	
	void setResult(String result) {
		this.result = result;
	}

}
