package com.peterchaschke.finances;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity(name = "transactions")
public class Transaction {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "institution_id")
	private long institutionId;
	
	@Column(name = "category_id")
	private long categoryId;
	
	@Column(name = "date")
	private String date;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "amount")
	private BigDecimal amount;
	
	@Column(name = "hash")
	private String hash;
	
	@Column(name = "note")
	private String note;
	
	@Transient
	private int internalId;
	
	@Transient
	private Boolean includeFlag;
	
	Transaction(long id, long institutionId, long categoryId, String date, String description, BigDecimal amount, String note) {
		this.setId(id);
		this.setInstitutionId(institutionId);
		this.setCategoryId(categoryId);
		this.setDate(date);
		this.setDescription(description);
		this.setAmount(amount);
		this.setNote(note);
	}
	
	Transaction(long institutionId, long categoryId, String date, String description, BigDecimal amount, String note, int internalId, boolean includeFlag) {
		this.setInstitutionId(institutionId);
		this.setCategoryId(categoryId);
		this.setDate(date);
		this.setDescription(description);
		this.setAmount(amount);
		this.setNote(note);
		this.setInternalId(internalId);
		this.setIncludeFlag(includeFlag);
	}
	
	Transaction(long id, long institutionId, String date, String description, BigDecimal amount) {
		this.setId(id);
		this.setInstitutionId(institutionId);
		this.setDate(date);
		this.setDescription(description);
		this.setAmount(amount);
	}
	
	Transaction(long institutionId, String date, String description, BigDecimal amount) {
		this.setInstitutionId(institutionId);
		this.setDate(date);
		this.setDescription(description);
		this.setAmount(amount);
	}
	
	Transaction(String date, String description, BigDecimal amount) {
		this.setDate(date);
		this.setDescription(description);
		this.setAmount(amount);
	}
	
	Transaction(){}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getInstitutionId() {
		return institutionId;
	}

	public void setInstitutionId(long institutionId) {
		this.institutionId = institutionId;
	}
	
	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}
	
	public String getNote() {
		return note;
	}
	
	public void setNote(String note) {
		this.note = note;
	}
	
	public int getInternalId() {
		return internalId;
	}
	
	public void setInternalId(int internalId) {
		this.internalId = internalId;
	}
	
	public Boolean getIncludeFlag() {
		return includeFlag;
	}
	
	public void setIncludeFlag(Boolean includeFlag) {
		this.includeFlag = includeFlag;
	}
	
	public void addAmount(BigDecimal addAmount) {
		this.setAmount(this.getAmount().add(addAmount));;
	}

}
