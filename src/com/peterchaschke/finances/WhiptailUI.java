package com.peterchaschke.finances;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ProcessBuilder.Redirect;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class WhiptailUI implements UI {
	
	/**
	 * Returns a menu selection from built from input string array list
	 * @param title
	 * @param message
	 * @param menuItems
	 * @param height
	 * @param width
	 * @param menuHeight
	 */
	public int getMenuSelection(String title, String message, ArrayList<String> menuItems, String height, String width, String menuHeight) {
		
		String[] process = {"whiptail", "--title", title, "--menu", message, height, width, menuHeight};
		
		ArrayList<String> processList = new ArrayList<>(Arrays.asList(process));

		// Build menu from input list.
		for(int i = 0; i < menuItems.size(); i++) {
			processList.add(menuItems.get(i));
		}
		
		// declaration and initialise String Array 
        process = new String[processList.size()]; 
  
        // Convert ArrayList to object array 
        Object[] objArr = processList.toArray(); 
  
        // Iterating and converting to String 
        int i = 0; 
        for (Object obj : objArr) { 
            process[i++] = (String)obj; 
        }
		
		try {
			ProcessResult result = executeProcess(process);
			
			// If process result was unsuccessful (cancelled) response with 1
			// If other errors, respond with -1
			if(result.getErrorCode() == 1) {
				return -1;
				
			} else {
				try {
					if(result.getResult().isEmpty()) {
						return -1;
					}
					return Integer.parseInt(result.getResult().trim());
					
				} catch(NumberFormatException e) {
					
					e.printStackTrace();
					return(-2);
				}
			}
			
		} catch (IOException | InterruptedException e) {
			
			e.printStackTrace();
			return(-2);
		}
	}
	
	/**
	 * Returns a date. Validates with input format, checks if prior or after a date (if provided).
	 * @param title
	 * @param message
	 * @param placeholder
	 * @param inputFormat
	 * @param before
	 * @param after
	 * @param height
	 * @param width
	 * @return
	 */
	public Date getInputDate(String title, String message, String placeholder, String inputFormat, Date beforeDate, Date afterDate, String height, String width) {
		
		Date date = null;
		String errorMsg = "";
		String originalMessage = message;
		int originalHeight = Integer.parseInt(height);
		
		boolean exitDialog = false;
		while(!exitDialog) {
			try {
				if(!errorMsg.isEmpty()) {
					message = originalMessage+"\n\n"+errorMsg;
					height = String.valueOf(originalHeight + 2);
				}
				String[] process = {"whiptail", "--title", title, "--inputbox", message, height, width, placeholder};
				
				ProcessResult result = executeProcess(process);
				
				// If process result was unsuccessful (cancelled) response with 1
				if(result.getErrorCode() == 1) {
					date = null;
					exitDialog = true;
					continue;
					
				} else {
					
					// Validate date is parseable
					date = new SimpleDateFormat(inputFormat).parse(result.getResult().trim());
					if(beforeDate != null) {
						// Validate date is not before provided date
						if(date.before(beforeDate)) {
							errorMsg = "Please enter a date on or after "+new SimpleDateFormat(inputFormat).format(beforeDate);
							exitDialog = false;
							continue;
						}
					}
					if(afterDate != null) {
						// Validate date is not after provided date
						if(date.after(afterDate)) {
							errorMsg = "Please enter a date on or before "+new SimpleDateFormat(inputFormat).format(afterDate);
							exitDialog = false;
							continue;
						}
					}
					// Valid date
					exitDialog = true;
					continue;

				}
			} catch (IOException | InterruptedException e) {
				
				date = null;
				exitDialog = true;
				continue;
				
			} catch (ParseException e) {
				
				//e.printStackTrace();
				errorMsg = "Error Parsing date, input format required ("+inputFormat+")";
				exitDialog = false;
				continue;
			}
		}
		return date;
		
	}
	
	/**
	 * Returns non empty string, or null on cancel
	 * @param title
	 * @param message
	 * @param placeholder
	 * @return
	 */
	public String getInputString(String title, String message, String placeholder, String height, String width, String errorMsg) {
		
		String originalMessage = message;
		int originalHeight = Integer.parseInt(height);
		String input = null;
		
		boolean exitDialog = false;
		while(!exitDialog) {
			try {
				if(!errorMsg.isEmpty()) {
					message = originalMessage+"\n\n"+errorMsg;
					height = String.valueOf(originalHeight + 2);
				}
				String[] process = {"whiptail", "--title", title, "--inputbox", message, height, width, placeholder};
				
				ProcessResult result = executeProcess(process);
				
				// If process result was unsuccessful (cancelled) response with 1, return null
				if(result.getErrorCode() == 1) {
					input = null;
					exitDialog = true;
					continue;
					
				} else {
					
					try {
						input = result.getResult().trim();
						if(input.length() <= 0) {
							continue;
						}
						return input;
						
					} catch(Exception e) {
						continue;
					}
				}
				
			} catch (IOException | InterruptedException e) {
				
				//e.printStackTrace();
				input = null;
				exitDialog = true;
				continue;
			}
		}
		return input;
	}
	
	/**
	 * Returns a big integer. Validated by lessThan or greaterThan
	 * @param title
	 * @param message
	 * @param placeholder
	 * @param smallerThan
	 * @param largerThan
	 * @param height
	 * @param width
	 * @return
	 */
	public BigDecimal getInputBigDecimal(
			String title, 
			String message, 
			String placeholder, 
			BigDecimal lessThan, 
			BigDecimal greaterThan, 
			BigDecimal equalTo, 
			String height, 
			String width) 
	{
		String errorMsg = "";
		String originalMessage = message;
		int originalHeight = Integer.parseInt(height);
		BigDecimal bigDecimal = null;
		
		boolean exitDialog = false;
		while(!exitDialog) {
			try {
				if(!errorMsg.isEmpty()) {
					message = originalMessage+"\n\n"+errorMsg;
					height = String.valueOf(originalHeight + 2);
				}
				String[] process = {"whiptail", "--title", title, "--inputbox", message, height, width, placeholder};
				
				ProcessResult result = executeProcess(process);
				
				// If process result was unsuccessful (cancelled) response with 1
				if(result.getErrorCode() == 1) {
					bigDecimal = null;
					exitDialog = true;
					continue;
					
				} else {
					
					try {
						if(result.getResult().isEmpty()) {
							errorMsg = "Please enter a value";
							continue;
						}
						// ENSURE VALUE IS GREATER THAN 0!
						bigDecimal = new BigDecimal(result.getResult().trim());
						
						//if(bigDecimal.compareTo(BigDecimal.ZERO) <= 0) {
						if(lessThan != null) {
							// True if input less than comparator
							if(bigDecimal.compareTo(lessThan) < 0) {
								errorMsg = "Input value must be greater than "+lessThan;
								continue;
							}
						}
						if(greaterThan != null) {
							// True if input greater than comparator
							if(bigDecimal.compareTo(greaterThan) > 0) {
								errorMsg = "Input value must be less than "+greaterThan;
								continue;
							}
						}
						if(equalTo != null) {
							// True if input greater than comparator
							if(bigDecimal.compareTo(equalTo) == 0) {
								errorMsg = "Input value must not equal "+equalTo;
								continue;
							}
						}
						// BigDecimal is valid
						exitDialog = true;
						continue;
						
					} catch(NumberFormatException e) {
						//e.printStackTrace();
						errorMsg = "Error parsing number. Ensure input it a number.";
						continue;
					}
				}
			} catch (IOException | InterruptedException e) {
				
				bigDecimal = null;
				exitDialog = true;
				continue;
				
			}
		}
		return bigDecimal;
		
	}
	
	/**
	 * Display yes/no dialog box
	 * Returns -1 if user selects no
	 * Returns 1 if user selects yes.
	 */
	public int displayYesNo(String title, String message) {
		
		String[] process = {"whiptail", "--title", title, "--yesno", message, "10", "60"};
		
		try {
			ProcessResult result = executeProcess(process);
		
		// If process result was unsuccessful (cancelled) response with 1
			// If other errors, respond with -1
			if(result.getErrorCode() == 1) {
				return -1;
				
			} else {
				try {
					if(result.getResult().isEmpty()) {
						return 1;
					}
					return Integer.parseInt(result.getResult().trim());
					
				} catch(NumberFormatException e) {
					
					e.printStackTrace();
					return(-2);
				}
			}
			
		} catch (IOException | InterruptedException e) {
			
			e.printStackTrace();
			return(-2);
		}
		
	}
	
	public int displayInformation(String message) {
		
		String[] process = {"whiptail", "--title", "Information", "--msgbox", message, "10", "60"};
		
		try {
			ProcessResult result = executeProcess(process);
		
		// If process result was unsuccessful (cancelled) response with 1
			// If other errors, respond with -1
			if(result.getErrorCode() == 1) {
				return -2;
				
			} else {
				try {
					if(result.getResult().isEmpty()) {
						return -1;
					}
					return Integer.parseInt(result.getResult().trim());
					
				} catch(NumberFormatException e) {
					
					e.printStackTrace();
					return(-1);
				}
			}
			
		} catch (IOException | InterruptedException e) {
			
			e.printStackTrace();
			return(-2);
		}
		
	}
	
	/**
	 * Returns a checklist selection array from built from input string array list
	 * @param title
	 * @param message
	 * @param checklistItems
	 * @param height
	 * @param width
	 * @param menuHeight
	 */
	public String[] getChecklistSelection(String title, String message, ArrayList<String> checklistItems, boolean selectionRequired, String height, String width, String menuHeight) {
		
		String errorMsg = "";
		String originalMessage = message;
		int originalHeight = Integer.parseInt(height);
        
        String[] selectedItems = null;
		
        boolean exitDialog = false;
		while(!exitDialog) {
			
			if(!errorMsg.isEmpty()) {
				message = originalMessage+"\n\n"+errorMsg;
				height = String.valueOf(originalHeight + 2);
			}
			
			String[] process = {"whiptail", "--title", title, "--checklist", message, height, width, menuHeight};
			
			ArrayList<String> processList = new ArrayList<>(Arrays.asList(process));

			// Build menu from input list.
			for(int i = 0; i < checklistItems.size(); i++) {
				processList.add(checklistItems.get(i));
			}
			
			// declaration and initialise String Array 
	        process = new String[processList.size()]; 
	  
	        // Convert ArrayList to object array 
	        Object[] objArr = processList.toArray(); 
	  
	        // Iterating and converting to String 
	        int i = 0; 
	        for (Object obj : objArr) { 
	            process[i++] = (String)obj; 
	        }
        
			try {
				ProcessResult result = executeProcess(process);
				
				// If process result was unsuccessful (cancelled) response with 1
				// If other errors, respond with -1
				if(result.getErrorCode() == 1) {
					return selectedItems;
					
				} else {
					
					String selectedItemsStr = result.getResult().trim();
					if(selectedItemsStr.trim().isEmpty()) {
						if(selectionRequired) {
							// A selection is required
							errorMsg = "A selection is required.";
							continue;
						} else {
							String[] emptyReturn = {""};
							return emptyReturn;
						}
					} else {
						// Split selected items string into array
						return selectedItemsStr.trim().split("\\s+");
					}		
				}
				
			} catch (IOException | InterruptedException e) {
				
				e.printStackTrace();
				return selectedItems;
			}
		}
		return selectedItems;
	}
	
	private static ProcessResult executeProcess(String[] process) throws IOException, InterruptedException {
		
		ProcessBuilder pb = new ProcessBuilder(process);
		
		pb.redirectOutput(Redirect.INHERIT);
		pb.redirectInput(Redirect.INHERIT);

		Process p = pb.start();

		int errorCode = p.waitFor();
		String output = "";
		
		// Cancelled dialog boxes will respond with a error code of 1
		if(errorCode == 0) {
			output = getOutput(p.getErrorStream());
		}
		return new ProcessResult(errorCode, output);
		
	}

	private static String getOutput(InputStream inputStream) throws IOException {
		StringBuilder sb = new StringBuilder();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(inputStream));
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line + System.getProperty("line.separator"));
			}
		} finally {
			br.close();
		}
		return sb.toString();
	}



}
