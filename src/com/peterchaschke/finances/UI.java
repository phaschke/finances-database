package com.peterchaschke.finances;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

public interface UI {
	
	int getMenuSelection(String title, String message, ArrayList<String> menuItems, String height, String width, String menuHeight);
	
	Date getInputDate(String title, String message, String placeholder, String inputFormat, Date beforeDate, Date afterDate, String height, String width);
	
	String getInputString(String title, String message, String placeholder, String height, String width, String errorMsg);
	
	BigDecimal getInputBigDecimal(String title, String message, String placeholder, BigDecimal lessThan, BigDecimal greaterThan, BigDecimal equalTo, String height, String width);
	
	int displayYesNo(String title, String message);
	
	int displayInformation(String message);
	
	String[] getChecklistSelection(String title, String message, ArrayList<String> checklistItems, boolean selectionRequired, String height, String width, String menuHeight);

}
