Finances Database

Phase 1 of a 2 part project to view and categorize credit/debit card transactions.

Linux/Unix terminal application. Features include:
Importing transaction csv files using specified file formats within the database.
Categorizing, removing, and splitting transactions before importing into the database.
Manually adding single transactions.
Exporting transactions by CSV within a daterange and by institutions.

Uses Java 8 w/Maven&Hibernate, MySQL, and whiptail(For UI).